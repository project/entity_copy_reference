<?php

namespace Drupal\Tests\content_editing_message\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test module.
 *
 * @group content_editing_message
 */
class ConfigFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_copy_reference',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createContentType(['type' => 'article']);

    $account =
            $this->drupalCreateUser([
              'administer entity_copy_reference',
              'use entity_copy_reference',
            ]);

    $this->drupalLogin($account);
  }

  /**
   * Test access to configuration page.
   */
  public function testCanAccessConfigForm() {
    $this->drupalGet('/admin/config/development/entity-copy-reference/config');
    $this->assertSession()->pageTextContains('Entity Copy with Reference Config');
  }

}
