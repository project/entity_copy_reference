<?php

namespace Drupal\entity_copy_reference;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\gsaml\GSAML;
use Drupal\node\Entity\Node;

/**
 * Class responsible for general Module operations.
 */
class EntityCopyReference {

  /**
   * Retrieves module config data (Registered in config form).
   *
   * Also checks for missing mandatory configs.
   */
  public function getConfig() {
    $config = [];

    $config = \Drupal::config('entity_copy_reference.settings')->get();

    if (empty($config['content_types'])) {
      \Drupal::logger('entity_copy_reference')->error('No copy settings were found');
      return [];
    }

    return $config;
  }

  /**
   * Function for checking if a user has permission to copy a given entity.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node to be checked.
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   The user to be checked. If NULL defaults to current user.
   *
   * @return bool
   *   Whether the user is allowed to copy given node
   */
  public function isCopyEnabled(Node $node, AccountProxyInterface $user = NULL) {
    if ($user == NULL) {
      $user = \Drupal::currentUser();
    }

    $config = $this->getConfig();

    if (
      !array_key_exists('content_types', $config)
      || !array_key_exists($node->bundle(), $config['content_types'])
    ) {
      return FALSE;
    }

    if (!$user->hasPermission('use entity_copy_reference')) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns a copy of the supplied node.
   */
  public function copyEntity($node, $is_referenced_entity = FALSE) {

    if (!is_object($node)) {
      $node = Node::load($node);
    }

    if (!empty($node)) {

      $config = $this->getConfig();

      $clone = $node->createDuplicate();

      // Overwrite relevant fields with new values.
      if ($clone->hasField('title')) {
        $prefix = $config['content_types'][$node->bundle()]['prefix'];
        $suffix = $config['content_types'][$node->bundle()]['suffix'];
        $clone->title = $prefix . $clone->title->value . $suffix;
      }

      // If not a paragraph and has field status, set it as unpublished
      // Paragraphs should remain published since we are already
      // unpublishing their containing entity.
      if ($clone->getEntityType()->id() !== 'paragraph' && $clone->hasField('status')) {
        $clone->status = FALSE;
      }

      $clone->created = time();
      $clone->changed = time();

      if (array_key_exists($node->bundle(), $config['content_types'])) {
        if (isset($config['content_types'][$node->bundle()]['reference_fields']) && !empty($config['content_types'][$node->bundle()]['reference_fields'])) {
          foreach ($config['content_types'][$node->bundle()]['reference_fields'] as $field => $option) {

            if (!$node->hasField($field)) {
              continue;
            }
            switch ($option) {
              // Case Copy referenced entities.
              case 1:
                $referenced_entities = $node->get($field)->referencedEntities();
                $new_entities = [];
                foreach ($referenced_entities as $referenced_entity) {

                  $referenced_entity_clone = $this->copyEntity($referenced_entity);
                  if ($referenced_entity_clone) {
                    $new_entities[] = $referenced_entity_clone;
                  }
                  else {
                    \Drupal::logger('entity_copy_reference')->error('Failed copying referenced entity');
                  }
                }

                $clone->$field = $new_entities;
                break;

              // Case Clear references.
              case 2:
                $new_entities = [];
                $clone->$field = $new_entities;
                break;

              default:
                // code...
                break;
            }
          }
        }
      }

    }

    try {
      if (
        $clone->save()
      ) {
        if (\Drupal::service('module_handler')->moduleExists('gsaml')) {
          GSAML::gsaml_entity_update($clone);
        }
        return [
          'target_id' => $clone->id(),
          'target_revision_id' => $clone->getRevisionId(),
        ];
      }
    }

    catch (\Exception $e) {
      \Drupal::logger('entity_copy_reference')->error('Exception on saving clone in Entity Copy with Reference\' copyEntity(): @ex', [
        '@ex' => $e->getMessage(),
      ]);
      return FALSE;
    }
  }

}
