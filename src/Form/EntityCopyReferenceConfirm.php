<?php

namespace Drupal\entity_copy_reference\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entity_copy_reference\EntityCopyReference;
use Drupal\node\Entity\Node;

/**
 * Class ImportForm.
 *
 * @package Drupal\entity_copy_reference\Form
 */
class EntityCopyReferenceConfirm extends FormBase implements FormInterface {

  /**
   * Set a var to make step-through form.
   *
   * @var step
   */
  protected $step = 1;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_copy_reference_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'entity_copy_reference/entity-copy-reference-form';

    $form['#title'] = $this->t('Entity Copy with Reference');

    $form['subtitle'] = [
      '#prefix' => '<h4>',
      '#markup' => '',
      '#suffix' => '</h4>',
    ];

    $node = \Drupal::routeMatch()->getParameter('node');
    $node = Node::load($node);
    // Check if this is a node of the types you want to handle.
    $form['subtitle']['#markup'] = t('Create copy of node <em>@node</em>?', ['@node' => $node->title->value]);

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create'),
      '#button_type' => 'primary',
    ];

    $edit_link = Url::fromRoute('entity.node.edit_form', ['node' => $node->id()])->toString();
    $form['cancel'] = [
      '#prefix' => '<a href="' . $edit_link . '" class="button">',
      '#markup' => $this->t('Cancel'),
      '#suffix' => '</a>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $node = \Drupal::routeMatch()->getParameter('node');
    if (!is_object($node)) {
      $node = Node::load($node);
    }
    $entity_copy_reference = new EntityCopyReference();
    $clone = $entity_copy_reference->copyEntity($node);

    $url = Url::fromRoute('entity_copy_reference.copy', ['node' => $node->id()]);
    if ($clone) {
      $url = Url::fromRoute('entity.node.edit_form', ['node' => $clone['target_id']]);
    }
    else {

      $messenger = \Drupal::messenger();
      $messenger->addMessage(
        t('An error occurred while copying node @node', ['@node' => $node->title->value]),
        $messenger::TYPE_ERROR
          );
      \Drupal::logger('entity_copy_reference')->error('There was an error copying node @node', ['@node' => $node->title->value]);
    }

    $form_state->setRedirectUrl($url);
  }

}
