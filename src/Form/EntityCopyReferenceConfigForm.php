<?php

namespace Drupal\entity_copy_reference\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ImportForm.
 *
 * @package Drupal\entity_copy_reference\Form
 */
class EntityCopyReferenceConfigForm extends FormBase implements FormInterface {
  /**
   * Set a var to make step-through form.
   *
   * @var step
   */
  protected $step = 1;

  /**
   * Set a var to save an array of content types selected for copy.
   *
   * @var selectedContentTypes
   */
  protected $selectedContentTypes = [];


  /**
   * Set a constant to populate field selects.
   *
   * @var FIELD_OPTIONS
   */
  const FIELD_OPTIONS = [
    0 => '0 - Keep Reference',
    1 => '1 - Clone Referenced Entity',
    2 => '2 - Clear Reference',
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_copy_reference_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'entity_copy_reference.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'entity_copy_reference/entity-copy-reference-form';

    $form['#title'] = $this->t('Entity Copy with Reference Config');

    $form['subtitle'] = [
      '#prefix' => '<h4>',
      '#markup' => '',
      '#suffix' => '</h4>',
    ];

    $config = $this->config('entity_copy_reference.settings');

    $submit_button = FALSE;
    switch ($this->step) {
      case 1:
        $form['subtitle']['#markup'] = t('Content Type Selection');

        $content_types = \Drupal::entityTypeManager()
          ->getStorage('node_type')
          ->loadMultiple();

        $form['content_types'] = [
          '#type' => 'details',
          '#title' => $this->t('Select which Content Types should have copy option added to menus and tasks'),
          '#open' => TRUE,
          '#tree' => TRUE,
        ];

        foreach ($content_types as $content_type) {

          $form['content_types'][$content_type->id()] = [
            '#type' => 'checkbox',
            '#title' => $content_type->label(),
            '#default_value' => ($config->get('content_types') && array_key_exists($content_type->id(), $config->get('content_types'))) ? 1 : 0,
          ];
        }

        $submit_button = $this->t('Next');
        break;

      case 2:
        $form['subtitle']['#markup'] = t('Reference Field Selection');

        if (!empty($this->selectedContentTypes)) {

          $form['content_types'] = [
            '#type' => 'details',
            '#title' => $this->t('Node Copy Config'),
            '#open' => TRUE,
            '#tree' => TRUE,
          ];

          $field_manager = \Drupal::service('entity_field.manager');

          foreach ($this->selectedContentTypes as $type) {

            $content_type = \Drupal::entityTypeManager()
              ->getStorage('node_type')
              ->load($type);
            // List all reference fields
            // If any of these are selected, referenced entities should
            // be cloned and referenced in new node copy.
            $form['content_types'][$type] = [
              '#type' => 'details',
              '#title' => $this->t(
                'Entity Reference Field in Content Type @content_type',
                [
                  '@content_type' => $content_type->label(),
                ]),
              '#open' => TRUE,
              '#tree' => TRUE,
            ];

            // Prefix field.
            $form['content_types'][$type]['prefix'] = [
              '#type' => 'textfield',
              '#title' => 'Copy Prefix for ' . $type,
              '#default_value' => (
                $config->get('content_types')
                && array_key_exists($type, $config->get('content_types'))
                && array_key_exists('prefix', $config->get('content_types')[$type])
              ) ? $config->get('content_types')[$type]['prefix'] : '',
            ];

            // Suffix field.
            $form['content_types'][$type]['suffix'] = [
              '#type' => 'textfield',
              '#title' => 'Copy Suffix for ' . $type,
              '#default_value' => (
                $config->get('content_types')
                && array_key_exists($type, $config->get('content_types'))
                && array_key_exists('suffix', $config->get('content_types')[$type])
              ) ? $config->get('content_types')[$type]['suffix'] : '',
            ];

            // Get list of all fields.
            $type_fields = $field_manager->getFieldDefinitions('node', $type);

            // Fields that should not be given as option.
            $ignore_fields = [
              'type',
              'revision_uid',
              'author',
              'uid',
            ];

            foreach ($type_fields as $field) {
              if (
              ($field->getType() == 'entity_reference' || $field->getType() == 'entity_reference_revisions')
              && !in_array($field->getName(), $ignore_fields)
              ) {
                $form['content_types'][$type]['reference_fields'][$field->getName()] = [
                  '#type' => 'select',
                  '#title' => $field->getLabel() . ' (' . $field->getName() . ')',
                  '#options' => self::FIELD_OPTIONS,
                  '#default_value' => (
                    $config->get('content_types')
                    && array_key_exists($type, $config->get('content_types'))
                    && array_key_exists($field->getName(), $config->get('content_types')[$type]['reference_fields'])
                  ) ? $config->get('content_types')[$type]['reference_fields'][$field->getName()] : 0,
                ];
              }
            }
          }
        }

        $submit_button = $this->t('Submit');
        break;
    }

    if ($submit_button) {
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $submit_button,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = \Drupal::configFactory()->getEditable('entity_copy_reference.settings');

    switch ($this->step) {
      case 1:

        if ($form_state->getValue('content_types')) {
          $this->selectedContentTypes = array_keys($form_state->getValue('content_types'), 1);
        }

        $this->step++;

        $form_state->setRebuild();
        break;

      case 2:

        if ($form_state->getValue('content_types')) {
          $content_types = [];

          foreach ($form_state->getValue('content_types') as $type => $configs) {

            $content_types[$type]['prefix'] = $form_state->getValue('content_types')[$type]['prefix'];
            $content_types[$type]['suffix'] = $form_state->getValue('content_types')[$type]['suffix'];

            foreach ($configs['reference_fields'] as $field => $option) {
              if ($option > 0) {
                $content_types[$type]['reference_fields'][$field] = $option;
              }
            }
          }

          if ($config->get('reference_fields')) {
            $config->clear('reference_fields');
          }
          $config->set('content_types', $content_types);
          $config->save();
        }

        $messenger = \Drupal::messenger();
        $messenger->addMessage(t('Entity Copy with Reference config updated successfully'), $messenger::TYPE_STATUS);
        $this->step = 1;
        $form_state->setRebuild();
        break;
    }
  }

}
