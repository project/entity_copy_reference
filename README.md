CONTENTS OF THIS FILE
----------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Maintainers


INTRODUCTION
------------

Entity Copy with Reference is a module made from the necessity of having 
a configurable easy to use one click duplication option

This module arose from the need to provide an easy way for content managers 
to copy nodes without having to fiddle with extensive configs.
Although a few modules for copying entities were available none quite fit 
the bill for what had been requested. Hence the creation of this module.

After installing this module you will be able to access the config form where 
existing Content Types will be listed. After selecting for which of those you 
want to make copying available a second page will be presented listing all 
reference fields on each of the selected types with options for how to treat 
each reference.

Currently you are given three options:

* Clear all references
* Keep the reference to the same entity as the original
* Duplicate the referenced entity as well

A common example would be paragraphs inside a node, 
in which case you would want to duplicate the paragraphs on copying a node 
so as to be able to change the new node's paragraphs 
without affecting the original.


REQUIREMENTS
------------

No requirements


INSTALLATION
------------

  1. Copy entity_copy_reference folder to modules directory
  2. At Administration >> Extend, enable the Entity Copy with Reference module.


CONFIGURATION
-------------

The configuration page for this module is at:
  Administration >> Configuration >> Development >> Entity Copy with Reference


MAINTAINERS
-----------

Maintainer: miguelpamferreira (https://www.drupal.org/u/miguelpamferreira)
